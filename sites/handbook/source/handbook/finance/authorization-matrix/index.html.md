---
layout: markdown_page
title: "Authorization Matrix"
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

For instruction on how to get approval to purchase goods or services see our [Procure to Pay Process](/handbook/finance/procure-to-pay/).
For instruction on how to get Sales contracts countersigned, please our [order processing - obtaining signatures process](/handbook/business-ops/order-processing/#obtain-signatures)

## Signing Legal Documents

The table below designates team members that are authorized to sign legal documents, with the exception of NDAs covering a physical visit of another organization.
When working with legal agreements with vendors, consultants, and so forth, bear in mind the [signature authorization matrix](/handbook/finance/authorization-matrix/#authorization-matrix).
If you need to obtain approval for a vendor contract, please create a confidential issue in the finance issue tracker using our [Contract Approval Workflow](/handbook/finance/procure-to-pay/).

For all other documents that need to be signed, filled out, sent, or retrieved electronically, please do your best to fill out the form using the handbook and [wiki](https://gitlab.com/gitlab-com/finance/wikis/company-information ) then e-mail it to `legal@` with the following information:

1. Names and email addresses of those who need to sign the document.
1. Any contractual information that needs to be included in the document.
1. Deadline (or preferred timeline) by which you need the document prepared (i.e. staged in [HelloSign](https://www.hellosign.com) for relevant signatures)
1. Include a link to the relevant issue in the body of the hellosign email message.
1. If the vendor insists on sending the document via electronic issue please provide them a link to include in the request.
1. Names and email addresses of those who need to be cc-ed on the signed document.

The process that Legal will follow is:

1. Review the document and prepare as requested.
1. Have the requestor check the prepared document, AND obtain approval from the CFO or CEO (such approval may be explicit in the email thread that was sent to `legal@`, in which case a second approval is not needed unless there have been significant edits to the document).
1. Requestor shall stage the document for signing in HelloSign and cc (at minimum) `legal@`.
1. Once signed: 
a. For customer contracts, the Requestor needs to attach the document to the applicable Contracts Object in Salesforce, and fill out all applicable fields in the Contracts Object. 
b. For vendor agreements, Requestor will [file the document in ContractWorks](/handbook/legal/vendor-contract-filing-process/).

## Authorization Matrix

*(All Functional Approvals require approval from previous tiers in hierarchy - Example: A CEO approval must also be approved by the exec team member prior to the CEO)*

Changes or amendments to the authorization matrix is approved by the CEO and CFO. If authority to the CEO is changed then board approval is required.

<div class="grid-auth-matrix">
  <div class="item-auth-matrix grid-empty"></div>
  <div class="item-auth-matrix" style="grid-column: 2/9"><b>Functional approval</b></div>
  <div class="item-auth-matrix" style="grid-column: 9/12"><b>Financial approval</b></div>
  <div class="item-auth-matrix"></div>
  <div class="item-auth-matrix"><b>Team Member</b></div> 
  <div class="item-auth-matrix"><b>Manager</b></div>  
  <div class="item-auth-matrix"><b>Director</b></div> 
  <div class="item-auth-matrix"><b>Sr Director/VP</b></div> 
  <div class="item-auth-matrix"><b>Exec Team</b></div>
  <div class="item-auth-matrix"><b>CEO</b></div>
  <div class="item-auth-matrix"><b>Board</b></div>  
  <div class="item-auth-matrix"><b>PAO or VP Finance</b></div>
  <div class="item-auth-matrix"><b>CFO</b></div>
  <div class="item-auth-matrix"><b>Director of Legal/CLO</b></div>
  <div class="item-auth-matrix" style="grid-column: 1/12"><b>Approving expense<sup>(1)(4)</sup></b></div>  
  <div class="item-auth-matrix">>$0K to $25K</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix">Approves</div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Approves/Signs</div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">>$25K up to $50K</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix">Approves</div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Approves/Signs</div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">>$50K up to $250K</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix">Approves</div>  <!-- Sr Director/VP -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Approves/Signs</div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">>$250K up to $500K</div> 
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix">Approves</div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix">Approves/Signs<sup>(5)</sup></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">$500K to $1M</div> 
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves</div>  <!-- CEO -->
  <div class="item-auth-matrix"></div> <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix">Signs</div> <!-- CFO -->
  <div class="item-auth-matrix"></div> <!-- Legal --> 
  <div class="item-auth-matrix">$1M+</div> 
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves<sup>(6)</sup></div>  <!-- CEO -->
  <div class="item-auth-matrix">Approves<sup>(2)</sup></div> <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix">Signs</div> <!-- CFO -->
  <div class="item-auth-matrix"></div> <!-- Legal --> 
  <div class="item-auth-matrix" style="grid-column: 1/12"><b>Approving Payment<sup>(1)</sup></b></div>
  <div class="item-auth-matrix">>$0K to $250K</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">X</div>  <!-- PAO -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">>$250K+</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO -->
  <div class="item-auth-matrix">X</div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix" style="grid-column: 1/12"><b>Legal Contracts</b></div>   
  <div class="item-auth-matrix">Standard Terms</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix">Signs</div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Signs</div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix">Signs/Approves</div>  <!-- Legal -->
  <div class="item-auth-matrix">Changes to Standard Terms and Vendor Contracts</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix">Approves</div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Signs</div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix">Signs/Approves</div>  <!-- Legal --> 
  <div class="item-auth-matrix">Non standard terms > $0.5M annual revenue	</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves</div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix">Signs</div> <!-- CFO -->
  <div class="item-auth-matrix">Signs/Approves</div>   <!-- Legal --> 
  <div class="item-auth-matrix">NDA - GitLab Template</div> 
  <div class="item-auth-matrix"></div>  
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix">Signs</div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div> <!-- Board -->
  <div class="item-auth-matrix">Signs</div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div> <!-- CFO -->
  <div class="item-auth-matrix">Signs/Approves</div> <!-- Legal --> 
  <div class="item-auth-matrix">NDA - Third Party</div> 
  <div class="item-auth-matrix"></div>  
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div> <!-- Board -->
  <div class="item-auth-matrix">Signs</div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div> <!-- CFO -->
  <div class="item-auth-matrix">Signs/Approves</div> <!-- Legal --> 
  <div class="item-auth-matrix" style="grid-column: 1/12"><b>Bad debt write off</b></div>   
  <div class="item-auth-matrix">Up to $10K</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix">Approves</div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Approves</div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">$10K to $100K</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves</div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Approves</div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal --> 
  <div class="item-auth-matrix">$100K plus</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves</div>  <!-- CEO -->
  <div class="item-auth-matrix">Advised</div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix">Approves</div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal --> 
  <div class="item-auth-matrix" style="grid-column: 1/12"><b>Compensation/Hiring - non-executive</b></div>   
  <div class="item-auth-matrix">Initial hiring Budgeted</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix">Approves</div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">Initial hiring Non-Budgeted</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves</div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Approves</div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal --> 
  <div class="item-auth-matrix">Use of Search Firm</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves</div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">Increases (Budgeted)</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix">Approves</div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Approves</div>  <!-- PAO or VP Finance -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">Increases (Not Budgeted)</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves</div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP, FP&A -->
  <div class="item-auth-matrix">Approves</div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal --> 
  <div class="item-auth-matrix">Bonuses</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves</div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP, FP&A -->
  <div class="item-auth-matrix">Approves</div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">Commission scales</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves</div>  <!-- CEO -->
  <div class="item-auth-matrix">Approves</div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP, FP&A -->
  <div class="item-auth-matrix">Approves</div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">Options</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Recommends</div>  <!-- CEO -->
  <div class="item-auth-matrix">Approves</div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP, FP&A -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal --> 
  <div class="item-auth-matrix">Executive Compensation - All</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix">Approves<sup>(3)</sup></div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP, FP&A -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix" style="grid-column: 1/12"><b>Benefit changes</b></div>   
  <div class="item-auth-matrix"></div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves</div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP, FP&A -->
  <div class="item-auth-matrix">Approves</div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix" style="grid-column: 1/12"><b>Employee Travel & Entertainment</b></div>   
  <div class="item-auth-matrix" style="grid-column: 1/12">Non Billable Expenses</div>  
  <div class="item-auth-matrix">Up to $5K</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix">Approves</div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Approves</div>  <!-- PAO or VP, FP&A -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal --> 
  <div class="item-auth-matrix">>$5K</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix">Approves</div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Approves</div>  <!-- PAO or VP, FP&A -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">>$50K</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix">Approves</div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP, FP&A -->
  <div class="item-auth-matrix">Approves</div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix" style="grid-column: 1/12"><b>Transfer of funds among GitLab entities</b></div> 
  <div class="item-auth-matrix">Up to $500K</div>
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix"></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix">Approves</div>  <!-- PAO or VP, FP&A -->
  <div class="item-auth-matrix"></div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
  <div class="item-auth-matrix">Over $500K	</div>  
  <div class="item-auth-matrix"></div>  <!-- Team Member -->
  <div class="item-auth-matrix"></div>  <!-- Manager -->
  <div class="item-auth-matrix"></div>  <!-- Director --> 
  <div class="item-auth-matrix"></div>  <!-- Sr Director -->  
  <div class="item-auth-matrix"></div>  <!-- Exec Team -->
  <div class="item-auth-matrix">Approves<sup>(5)</sup></div>  <!-- CEO -->
  <div class="item-auth-matrix"></div>  <!-- Board -->
  <div class="item-auth-matrix"></div>  <!-- PAO or VP, FP&A -->
  <div class="item-auth-matrix">Approves</div>  <!-- CFO -->
  <div class="item-auth-matrix"></div>  <!-- Legal -->
</div>

<div class="treasury-auth-matrix">
  <div class="tr-auth-matrix grid-empty"></div>
  <div class="tr-auth-matrix"><b>Board</b></div>
  <div class="tr-auth-matrix">Letters of Credit</div>
  <div class="tr-auth-matrix">Over $2.5M - Approval required</div>
  <div class="tr-auth-matrix">Guarantees</div>
  <div class="tr-auth-matrix">Over $2.5M - Approval required</div>
  <div class="tr-auth-matrix">Debt/Loan Financings</div>
  <div class="tr-auth-matrix">Approves</div>
  <div class="tr-auth-matrix">Loan Renewals</div>
  <div class="tr-auth-matrix">Approves</div>
  <div class="tr-auth-matrix">Investment Policy</div>
  <div class="tr-auth-matrix">Approves</div>
  <div class="tr-auth-matrix">Equity Financings</div>
  <div class="tr-auth-matrix">Approves</div>
  <div class="tr-auth-matrix">Changing Banks</div>
  <div class="tr-auth-matrix">No Approval required; covered under Opening or Closing Bank Accounts</div>  
  <div class="tr-auth-matrix">Opening or Closing Bank Accounts</div>
  <div class="tr-auth-matrix">Chief Executive Officer approval (Delegation of approval authority to CEO to be recommended to the full BOD)</div>
  <div class="tr-auth-matrix">Establish, liquidate, or change the legal status of an entity</div>
  <div class="tr-auth-matrix">Approves</div>
  <div class="tr-auth-matrix">Acquire an entity</div>
  <div class="tr-auth-matrix">Over $2.5M - Approval required</div>
  <div class="tr-auth-matrix">Decisions to file a lawsuit or accept an injunction or consent decree (other than collection of receivables in due course)</div>
  <div class="tr-auth-matrix">Chief Legal Officer Approves</div>
  <div class="tr-auth-matrix">Decision to withdraw from/settle a lawsuit >$250K</div>
  <div class="tr-auth-matrix">Approves</div>
  <div class="tr-auth-matrix">Hiring or terminating corporate counsel</div>
  <div class="tr-auth-matrix">No Approval required</div>
  <div class="tr-auth-matrix">Amendment to subsidiary charter or by-laws</div>
  <div class="tr-auth-matrix">No Approval required</div>
  <div class="tr-auth-matrix">Change to equity structure and option plans</div>
  <div class="tr-auth-matrix">Approves</div>
  <div class="tr-auth-matrix">Severance payments greater than 4 weeks of severance payments or the amount of severance payment required by local law or regulation which are not provided for in Company policies or plans</div>
  <div class="tr-auth-matrix">Chief Legal Officer Approves</div>
</div>


**Notes/Comments**:

- (1) Delegation of authority not to exceed 90 days in duration is permitted. Written notice of delegation shall be provided by the  delegating team member to the CFO,  CLO,  PAO and Director of Legal, Corporate in order for the delegation to be effective.
- (2) If not included in Company Plan.
- (3) Approved by Compensation Committee.
- (4) Approval from [function](/company/team/structure/#table) responsible for supporting associated objectives and goals.
- (5) Only applies to approvals outside of the discount authorization matrix.
- (6) For cloud hosting provider committed usage discounts (CUD) - approvals are delegated to VP Infrastructure and VP Finance

## Banking Controls

- All accounts are to be established so initiator and approver must be different.

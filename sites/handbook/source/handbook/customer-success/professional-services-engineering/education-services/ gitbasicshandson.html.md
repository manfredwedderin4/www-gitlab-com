---
layout: handbook-page-toc
title: "GitLab with Git Basics Hands On Guide"
description: "This Hands On Guide is designed to walk you through the lab exercises used in the GitLab with Git Basics course."
---
# GitLab with Git Basics Hands On Guide
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Hands On Lab Setup

1. To begin lab, navigate to the sandbox link: <https://www.gitlabdemo.com/invite>  
2. Enter your invitation code in the box and Click **Redeem**. 
3. You will be brought to the link below: 
4. Click **Download Credentials**. 
5. Click the GitLab URL and launch your demo environment. 
6. Enter your credentials and launch the demo environment. 

## GitLab with Git Basics Labs
* [Lab 1- Create a Project and an Issue](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/%20gitbasicshandsonlab1.html)
* [Lab 2- Work with Git Locally](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/%20gitbasicshandsonlab2.html)
* [Lab 3- Use GitLab to Push Code](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/%20gitbasicshandsonlab3.html)
* [Lab 4- Build a Gitlab-ci.yml File](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/%20gitbasicshandsonlab4.html)
* [Lab 5- Use an Auto DevOps Predefined Template](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/%20gitbasicshandsonlab5.html)
* [Lab 6- Use Security Scanning (SAST)](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/%20gitbasicshandsonlab6.html)

## Quick links

Here are some quick links that may be useful when reviewing this Hands On Guide.

* [GitLab Demo Cloud](https://gitlabdemo.com/login)
* [GitLab with Git Basics Course Description](https://about.gitlab.com/services/education/gitlab-basics/)
* [GitLab Certified Associate Certifcation Details](https://about.gitlab.com/services/education/gitlab-certified-associate/)


### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab with Git Basics- please submit your changes via Merge Request!

